<?php
/**
 * Plugin Name: Register by Duc An
 * Plugin URI: uli.edu.vn // Địa chỉ trang chủ của plugin
 * Description: Đây là plugin đầu tiên mà tôi viết dành riêng cho WordPress, chỉ để học tập mà thôi.
 * Version: 1.0
 * Author: josducan216 
 * Author URI: fb.com/josducan216 
 * License: GPLv2 or later 
 */
?>
<?php
function registration_form( $fullname, $age, $email, $address, $phonenumber) {
    echo '
    <style>
    div {
      margin-bottom:2px;
    }
     
    input{
        margin-bottom:4px;
    }
    </style>
    ';
 
    echo '
    <form action="' . $_SERVER['REQUEST_URI'] . '" method="post">
    
    <div>
    <label for="fullname"></label>
    <input type="text" name="fullname" placeholder="Họ tên" value="' . ( isset( $_POST['fullname']) ? $fullname : "" ) . '">
    </div>

    <div>
    <label for="age"></label>
    <input type="text" name="age" placeholder="Độ tuổi" value="' . ( isset( $_POST['age']) ? $age : "" ) . '">
    </div>

    <div>
    <label for="email"></label>
    <input type="text" name="email" placeholder="Email" value="' . ( isset( $_POST['email']) ? $email : "" ) . '">
    </div>

    <div>
    <label for="address"></label>
    <input type="text" name="address" placeholder="Bạn đang sống tại" value="'.(isset($post['address'])? $address : "").'">
    </div>

    <div>
    <label for="phonenumber"></label>
    <input type="text" name="phonenumber" placeholder="Số điện thoại" value="'.(isset($post['phonenumber'])? $phonenumber : "").'">
    </div>
    
    <input type="submit" name="submit" value="ĐĂNG KÍ"/>

    </form>
    ';
}
?>

<?php
function complete_registration() {
    global $reg_errors, $fullname, $age, $email, $address, $phonenumber;
    if ( 1 > count( $reg_errors->get_error_messages() ) ) {
        $userdata = array(
        'user_login'    =>   $username,
        'user_email'    =>   $email,
        'user_pass'     =>   $password,
        'user_url'      =>   $website,
        'first_name'    =>   $first_name,
        'last_name'     =>   $last_name,
        'nickname'      =>   $nickname,
        'description'   =>   $bio,
        );
        $user = wp_insert_user( $userdata );
        echo 'Registration complete. Goto <a href="' . get_site_url() . '/wp-login.php">login page</a>.';   
    }
}
?>

<?php
function custom_registration_function() {
    if ( isset($_POST['submit'] ) ) {
        registration_validation(
        $_POST['username'],
        $_POST['password'],
        $_POST['email'],
        $_POST['website'],
        $_POST['fname'],
        $_POST['lname'],
        $_POST['nickname'],
        $_POST['bio']
        );
         
        // sanitize user form input
        global $username, $password, $email, $website, $first_name, $last_name, $nickname, $bio;
        $username   =   sanitize_user( $_POST['username'] );
        $password   =   esc_attr( $_POST['password'] );
        $email      =   sanitize_email( $_POST['email'] );
        $website    =   esc_url( $_POST['website'] );
        $first_name =   sanitize_text_field( $_POST['fname'] );
        $last_name  =   sanitize_text_field( $_POST['lname'] );
        $nickname   =   sanitize_text_field( $_POST['nickname'] );
        $bio        =   esc_textarea( $_POST['bio'] );
 
        // call @function complete_registration to create the user
        // only when no WP_error is found
        complete_registration(
        $username,
        $password,
        $email,
        $website,
        $first_name,
        $last_name,
        $nickname,
        $bio
        );
    }
 
    registration_form(
        $username,
        $password,
        $email,
        $website,
        $first_name,
        $last_name,
        $nickname,
        $bio
        );
}
?>

<?php
// Register a new shortcode: [cr_custom_registration]
add_shortcode( 'cr_custom_registration', 'custom_registration_shortcode' );
 
// The callback function that will replace [book]
function custom_registration_shortcode() {
    ob_start();
    custom_registration_function();
    return ob_get_clean();
}
?>

