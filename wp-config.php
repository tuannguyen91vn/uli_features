<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ULI-demo');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'F`wL-CNLloDdC3:+!Mpv;a[N1*W(2@};F5tDZasAb114ybV=t{MLsbP5e2s0`3Qq');
define('SECURE_AUTH_KEY',  ']YkPY@8)Ckfl_Ib*T`q?>:U`gHbCcA)wOk{PKbq FdV{qyu@B-%a_7khpkXU=$}n');
define('LOGGED_IN_KEY',    'U[zYq}qF{YUDo8xhg|W]k1a1Z?OY[Hto>IY5VRE&Wq8%AKZ8*_T?]3Ywz%-K#@n6');
define('NONCE_KEY',        'z]}nu)Q%2Z2^r4|m43JP&yv4#1@``hW/M*H4p&[kT9FrC#.%hKSOT5Q$2^0W`GC.');
define('AUTH_SALT',        '&GBP?Y?8Yh=5aZFu!VBX<*u}3j]Q#q57jJpEBsU-r@@L>.t[n=W~Bev#d,[$:F2}');
define('SECURE_AUTH_SALT', '~Z:3Ccqp=U*L~3lcA=.?q4jUaA$2UF0.#U$;{Gs36n(IJ#1QE~n@(dJJJ ##n:-F');
define('LOGGED_IN_SALT',   '(!Dzqt6Kv;Q#DI6OmSH^f+M5#_:C &N{x;JMr$jQl]xwks*hPDa;5MVsJmQ_*cAB');
define('NONCE_SALT',       'z.)KF.;%WLT.;*IrZy,Jwy&(xsaYDk7jG+^5%[%&YJc.a cpS/Q8T1rGC <C1/AA');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'ULI';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
